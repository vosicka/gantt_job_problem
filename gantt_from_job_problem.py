#Program na kapacitní plánování

# Importy knihoven
import collections
import ortools.sat.python.cp_model
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

#Vytvoření formátu

# Třída reprezentující operace (task)
class Task:
    # Definice vytvoření nové operace
    def __init__(self, start: object, interval: object, end: object):
        # Nastavení hodnot pro proměnné
        self.start = start
        self.interval = interval
        self.end = end


# Třída reprezentující úkol (assignment)
class Assignment:
    # Definice vytvoření nového úkolu
    def __init__(self, job_id: int, task_id: int, start: int, duration: int):
        # Nastavení hodnot pro proměnné
        self.job_id = job_id
        self.task_id = task_id
        self.start = start
        self.duration = duration

    # Uspořádání (sort)
    def __lt__(self, other):
        return self.start + self.duration < other.start + other.duration

    # Vypsání (print)
    def __repr__(self):
        return ('(Job: {0}, Task: {1}, Start: {2}, End: {3})'.format(self.job_id, self.task_id, self.start,
                                                                     self.start + self.duration))


# Zadání funkce modulu
def Minimalizace_casovych_prodlev():

# Vstupy dat: Operace = (stroj_id, trvání) [Input data: Task = (machine_id, duration)]
    zakazky = [
        [(2, 3, ()), (0, 7, (0,)), (1, 17, (1,)), (0, 5, ()), (2, 16, (3,)), (1, 12, (4,)), (0, 5, ()), (1, 3, (6,)), (2, 4, (7,)), (2, 14, (2, 5, 8)), (1, 8, (9,)), (0, 0, (10,))], # zakázka 0
        [(1, 7, ()), (0, 4, ()), (3, 1, (1,)), (2, 6, (2,)), (4, 1, (3,)), (1, 8, (4,)), (2, 16, (0, 5)), (1, 7, (6,)), (0, 0, (7,))] # zakázka 1
    ]

# Definice proměnných (variables)
    pocet_stroju = 1 + max(task[0] for job in zakazky for task in job)
    tasks = {}
    intervals = collections.defaultdict(list)
    assignments = collections.defaultdict(list)
    # Výpočet možného času - suma všech délek trvání [Compute horizon dynamically (sum of all durations)]
    horizon = sum(task[1] for job in zakazky for task in job)
    # Vytvoření modelu
    model = ortools.sat.python.cp_model.CpModel()
    # Smyčka (loop) zakazky
    for job_id, job in enumerate(zakazky):
        # Smyčka (loop) tasks in a job
        for task_id, task in enumerate(job):
            # Proměnné (variables)
            machine_id = task[0]
            duration = task[1]
            suffix = '_{0}_{1}'.format(job_id, task_id)
            # Vytvoření modelu proměnných (create model variables)
            start = model.NewIntVar(0, horizon, 'start' + suffix)
            end = model.NewIntVar(0, horizon, 'end' + suffix)
            interval = model.NewIntervalVar(start, duration, end, 'interval' + suffix)
            # Přidání operace (Add a task)
            tasks[job_id, task_id] = Task(start, interval, end)
            # Přidání intervalu pro stroje (add an interval for the machine)
            intervals[machine_id].append(interval)

# Definice omezení (constraints)
    # Přidání omezení - žádné překrývání na stroji (pracovišti) (add no-overlap constraints - a machine can only work with 1 task at a time)
    for machine in range(pocet_stroju):
        model.AddNoOverlap(intervals[machine])
    # Přidání omezení - posloupnost operací (add precedence constraints - Tasks in a job must be performed in the specified order)
    for job_id, job in enumerate(zakazky):
        for task_id in range(len(job) - 1):
            for SerialNumber in job[task_id][2]:
                model.Add(tasks[job_id, SerialNumber].end <= tasks[job_id, task_id].start)

# Definice výstupů
    # Vytvoření účelové funkce (create an objective function)
    objective = model.NewIntVar(0, horizon, 'makespan')
    model.AddMaxEquality(objective, [tasks[job_id, len(job) - 1].end for job_id, job in enumerate(zakazky)])
    model.Minimize(objective)
    # Vytvořte řešitele (create a solver)
    solver = ortools.sat.python.cp_model.CpSolver()
    # Nastavte časový limit 30 sekund (set a time limit of 30 seconds)
    solver.parameters.max_time_in_seconds = 30.0
    # Vyřešení problému (Solve the problem)
    status = solver.Solve(model)
    # Vypsání výsledku pokud je řešení optimální (print output if the solution is optimal)
    if (status == ortools.sat.python.cp_model.OPTIMAL):
        # Smyčka (loop) zakazky
        for job_id, job in enumerate(zakazky):
            # Smyčka (loop) tasks in a job
            for task_id, task in enumerate(job):
                # Přidání úkolu (add an assignment)
                machine_id = task[0]
                start = solver.Value(tasks[job_id, task_id].start)
                assignments[machine_id].append(Assignment(job_id, task_id, start, task[1]))
        # Vytváření mříže a seřazení úkolů (create bars and sort assignments)
        bars = []
        for machine in range(pocet_stroju):
            assignments[machine].sort()
            bar_tasks = []
            for ass in assignments[machine]:
                bar_tasks.append((ass.start, ass.duration))
            bars.append(bar_tasks)

# Vypsání řešení (print the solution)
        print('--- Final solution ---\n')
        print('Optimal Schedule Length: {0}\n'.format(solver.ObjectiveValue()))
        print('Schedules:')
        for machine in range(pocet_stroju):
            print(machine, ':', *assignments[machine])
        print()

# Vykreslení Ganttova diagramu (plot gantt chart)
        fig, gnt = plt.subplots(figsize=(12, 8))
        fig.suptitle('Plán výroby', fontsize=16)
        gnt.set_xlabel('Čas [min.]')
        gnt.set_ylabel('Stroje/Pracoviště')
        gnt.set_yticks([12, 22, 32, 42, 52])
        gnt.set_yticklabels(['Horizontální fréza', 'Karusel', 'Svařování', 'Mechanici', 'Ruční broušení'])
        gnt.grid(True)
        # Smyčka (loop) mřížky
        for i in range(len(bars)):
            gnt.broken_barh(bars[i], (10 + i * 10, 4), facecolors=('tab:orange', 'tab:green'))
            j = 0
            for x1, x2 in bars[i]:
                gnt.text(x=x1 + x2 / 2, y=12 + i * 10, s=j, ha='center', va='center', color='white')
                j += 1
        # Vytvoření legendy (create a legend)
        labels = []
        labels.append(mpatches.Patch(color='tab:orange', label='Task 0'))
        labels.append(mpatches.Patch(color='tab:green', label='Task 1'))
        #labels.append(mpatches.Patch(color='tab:red', label='Task 2'))
        plt.legend(handles=labels, loc=4)
        # Zobrazení vykresleného grafu (show the plot)
        plt.show()
        # Uložení vykresleného grafu (save the plot)
        #plt.savefig('./schedule-gantt.png')


# Vyvolání řešení
if __name__ == '__main__': Minimalizace_casovych_prodlev()

